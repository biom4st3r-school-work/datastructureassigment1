// Write to file "109"
#include <stdio.h>
#include <string.h>
#define FILE0 "Curtis.txt"
#define FILE1 "Harris.txt"
#define ABC "109"
// #define BUFFSZ 256


int main(int count, char** args) {
  FILE* file = fopen(FILE0, "r");
   printf("Assignment %s: ", "CC4"  );
  if (file == NULL) return -1;
  fwrite(ABC, sizeof(char), strlen(ABC), file);
  fclose(file);
  printf("Wrote '%s' to file %s\n", ABC, FILE0);


  return 0;
}