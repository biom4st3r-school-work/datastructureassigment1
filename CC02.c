// Open and close existing file
#include <stdio.h>
#define FILE0 "Curtis.txt"
#define FILE1 "Harris.txt"

int main(int count, char **args) {
  FILE *file = fopen(FILE0, "a");
  printf("Assignment %s: ", "CC2");
  if (file == NULL)
    return -1;
  fclose(file);
  printf("File %s openned and closed\n", FILE0);
  return 0;
}