// Convert string to uppercase(last name)

#include <ctype.h>
#include <stdio.h>
#include <string.h>
int main(int count, char **args) {
  printf("Assignment %s: ", "CC10");
  char lastname[7] = "Harris";
  printf("%s to uppercase is ", lastname);
  for (int i = 0; i < strlen(lastname); i++) {
    lastname[i] = toupper(lastname[i]);
  }
  printf("%s\n", lastname);
  return 0;
}