// Seek to position in file
#include <stdio.h>
#include <string.h>
#define FILE0 "Curtis.txt"
#define FILE1 "Harris.txt"
#define ABC "109"
// #define BUFFSZ 256

int main(int count, char **args) {
  FILE *file = fopen(FILE0, "r");
  printf("Assignment %s: ", "CC5");
  if (file == NULL)
    return -1;
  fseek(file, 2, SEEK_SET);
  fclose(file);
  printf("Seeked in file %s\n", FILE0);

  return 0;
}