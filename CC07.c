// Concat 2 strings(First and last name)
#include <stdio.h>
#include <string.h>
#define FILE0 "Curtis.txt"
#define FILE1 "Harris.txt"
#define ABC "109"
// #define BUFFSZ 256

int main(int count, char **args) {
  printf("Assignment %s: ", "CC7");
  char *given = "Curtis";
  char *family = "Harris";

  char combined[25];
  strcpy(combined, given);
  strcat(combined, family);
  printf("%s concated with %s is %s\n", given, family, combined);
  return 0;
}