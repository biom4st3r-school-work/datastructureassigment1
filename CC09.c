// Convert string to lowercase(lastname)

#include <ctype.h>
#include <stdio.h>
#include <string.h>
int main(int count, char **args) {
  printf("Assignment %s: ", "CC9");
  char lastname[7] = "Curtis";
  // strcpy(lastname, "Curtis");
  printf("%s to lowercase is ", lastname);
  for (int i = 0; i < strlen(lastname); i++) {
    lastname[i] = tolower(lastname[i]);
  }
  printf("%s\n", lastname);
  return 0;
}