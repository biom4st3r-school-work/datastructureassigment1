// Read from file
#include <stdio.h>
#define FILE0 "Curtis.txt"
#define FILE1 "Harris.txt"
#define BUFFSZ 256

int main(int count, char **args) {
  FILE *file = fopen(FILE0, "r");
  printf("Assignment %s: ", "CC3");

  if (file == NULL)
    return -1;
  char buffer[BUFFSZ];
  if (fgets(buffer, BUFFSZ, file) == 0) {
    printf("Read no bytes from file %s\n", FILE0);
    return -1;
  }
  fclose(file);
  printf("File %s read -> %s\n", FILE0, buffer);
  return 0;
}